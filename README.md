# Convert excel file to csv file
[![Crates.io](https://img.shields.io/crates/v/loa.svg)](https://crates.io/crates/base64_text)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/base64_text)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/base64_text/-/raw/master/LICENSE)

## install

```sh
cargo install base64_text
```

```bash
Base64 Encoder/Decoder 1.0
Addrew Ryan <dnrops@outlook.com>
A CLI tool for encoding and decoding Base64 strings

USAGE:
    base64_text.exe [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -d, --decode <STRING>    Decode a string from Base64 format
    -e, --encode <STRING>    Encode a string in Base64 format
```
