use base64::{decode, encode};
use clap::{App, Arg};

fn main() {
    let matches = App::new("Base64 Encoder/Decoder")
        .version("1.0")
        .author("Addrew Ryan <dnrops@outlook.com>")
        .about("A CLI tool for encoding and decoding Base64 strings")
        .arg(
            Arg::with_name("encode")
                .short("e")
                .long("encode")
                .value_name("STRING")
                .help("Encode a string in Base64 format")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("decode")
                .short("d")
                .long("decode")
                .value_name("STRING")
                .help("Decode a string from Base64 format")
                .takes_value(true),
        )
        .get_matches();

    if let Some(input) = matches.value_of("encode") {
        let encoded = encode(input);
        println!("{}", encoded);
    } else if let Some(input) = matches.value_of("decode") {
        match decode(input){
            Ok(r)=>{
                println!("{}", String::from_utf8(r).unwrap());
            },
            Err(e)=>{
                println!("{}",e);
            }
        }
    }
}
